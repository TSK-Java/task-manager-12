package ru.tsc.kirillov.tm;

import ru.tsc.kirillov.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
